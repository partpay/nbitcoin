FROM mcr.microsoft.com/dotnet/sdk:6.0-jammy AS builder

RUN apt update && apt install -y libcurl4 libgit2-dev

WORKDIR /source
COPY . .

VOLUME /out

CMD dotnet --list-sdks \
    && dotnet build -c Release NBitcoin || true \
    && dotnet pack --no-build -c Release -o /out/packages NBitcoin \
    && dotnet build -c Release NBitcoin.Altcoins || true \
    && dotnet pack --no-build -c Release -o /out/packages NBitcoin.Altcoins
